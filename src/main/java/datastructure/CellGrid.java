package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[this.rows][this.cols];

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    // Because we use CellState[][] instead of ArrayList<Cellstate>
    // (Or another way), both set() and get() will throw
    // `IndexOutOfBoundsException`s automatically if any of the
    // indexes are invalid.
    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid returnGrid = new CellGrid(rows, cols, null);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                returnGrid.set(i, j, grid[i][j]);
            }
        }
        return returnGrid;
    }

}
