package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * BriansBrain class
 * Implements a "brian's brain" CellAutomaton
 * (This class has the same contents except for the game rules)
 * 
 * (Though, i thought we were supposed to avoid duplicating code?
 * Maybe it would be better to create an abstract class that both
 * inherits from and only overrides the rules...
 * but the README said that it must start with the following line...)
 */ 
public class BriansBrain implements CellAutomaton {
	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Brians Brain Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}

		currentGeneration = nextGeneration.copy();
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState currState = getCellState(row, col);
		int neighborsAlive = countNeighbors(row, col, CellState.ALIVE);
        
		if (currState.equals(CellState.ALIVE)) {
			return CellState.DYING;
		} else if (currState.equals(CellState.DYING)) {
            return CellState.DEAD;
        } else if (currState.equals(CellState.DEAD)) {
			if (neighborsAlive == 2)
				return CellState.ALIVE;
		}
		return currState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;
		
		// Check neighbors around the selected cell, like so ((0,0) origin):
		// |-1,-1|-1, 0|-1, 1|
		// | 0,-1| 0, 0| 0, 1|
		// | 1,-1| 1, 0| 1, 1|
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (onGrid(row + i, col + j) && !(i == 0 && j == 0)) {
					if (getCellState(row + i, col + j).equals(state)) {
						neighbors++;
					}
				}
			}
		}

		return neighbors;
	}

	// Checks if the given cell is valid
	// (if it doesn't throw an exception)
	private boolean onGrid(int row, int col) {
		try {
			currentGeneration.get(row, col);
			return true;
		// Is this a hack? It does work though...
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
